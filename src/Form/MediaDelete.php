<?php

namespace Drupal\media_delete_all\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create a Form for deleting all content.
 */
class MediaDelete extends FormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\media_delete_all\MediaDeleteService definition.
   *
   * @var \Drupal\media_delete_all\MediaDeleteService
   */
  protected $mediaDeleteAll;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->mediaDeleteAll = $container->get('media_delete.all');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_delete_all';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['select_all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete All Type'),
      '#description' => $this->t('Delete all media of all type'),
    ];
    $form['type_details'] = [
      '#type' => 'details',
      '#title' => $this->t('Media types'),
      '#description' => $this->t('Select the types of media content to delete'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="select_all"]' => ['checked' => FALSE],
        ],
      ],
    ];
    $form['type_details']['media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select The Media Type'),
      '#options' => $this->getAvailableMediaType(),
      '#states' => [
        'visible' => [
          ':input[name="select_all"]' => ['checked' => FALSE],
        ],
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
    ];
    return $form;
  }

  /**
   * It return the availble Media type present in system.
   */
  public function getAvailableMediaType() {
    $mediaTypes = $this->entityTypeManager->getStorage('media_type')->loadMultiple();
    $mediaTypesList = [];
    foreach ($mediaTypes as $mediaType) {
      $mediaTypesList[$mediaType->id()] = $mediaType->label();
    }
    return $mediaTypesList;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $delete_all_checkbox = $form_state->getValue(['select_all']);
    $all_media_type = array_keys($this->getAvailableMediaType());
    $selected_media_type = $form_state->getValue('media_type');

    if ($delete_all_checkbox == 1) {
      $media_to_delete = $this->mediaDeleteAll->getMediaToDelete($all_media_type);
    }
    else {
      $media_to_delete = $this->mediaDeleteAll->getMediaToDelete([$selected_media_type]);
    }

    if ($media_to_delete) {
      $batch = $this->mediaDeleteAll->getMediatDeleteBatch($media_to_delete);
      batch_set($batch);
    }
    else {
      $this->messenger()->addMessage($this->t('No Media found'));
    }
  }

}
