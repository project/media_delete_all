<?php

namespace Drupal\media_delete_all;

use Drupal\Core\Database\Driver\mysql\Connection;

/**
 * Class MediaDeleteService.
 */
class MediaDeleteService {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * Constructs a new MediaDeleteService object.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * Get mids of the media to delete.
   *
   * @param array $media_types
   *   Array of media_types.
   *
   * @return array
   *   Array of mids of medias to delete.
   */
  public function getMediaToDelete(array $media_types = NULL) {
    $medias_to_delete = [];
    // Delete media  by media type.
    if (!empty($media_types)) {
      foreach ($media_types as $media_type) {
        if ($media_type) {
          $mids = $this->database->select('media', 'm')
            ->fields('m', ['mid'])
            ->condition('bundle', $media_type)
            ->execute()
            ->fetchCol('mid');

          $medias_to_delete = array_merge($medias_to_delete, $mids);
        }
      }
    }
    // Delete all content.
    else {
      $medias_to_delete = NULL;
    }

    return $medias_to_delete;
  }

  /**
   * Delete media batch.
   *
   * @param array $medias_to_delete
   *   Array of medias_to_delete.
   *
   * @return array
   *   Array of batch of medias to delete.
   */
  public function getMediatDeleteBatch(array $medias_to_delete = NULL) {
    // Define batch.
    $batch = [
      'operations' => [
        [
          '\Drupal\media_delete_all\DeleteMediaBatchController::deleteAllMediaBatchDelete',
          [$medias_to_delete],
        ],
      ],
      'finished' => '\Drupal\media_delete_all\DeleteMediaBatchController::deleteAllMediaBatchDeleteFinished',
      'title' => 'Deleting Media',
      'init_message' => 'Media deletion is starting.',
      'progress_message' => 'Deleting Media...',
      'error_message' => 'Media deletion has encountered an error.',
    ];
    return $batch;
  }

}
