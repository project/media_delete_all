<?php

namespace Drupal\media_delete_all\Commands;

use Drush\Commands\DrushCommands;
use Drush\Exceptions\UserAbortException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class DeleteAllCommands extends DrushCommands {

  /**
   * Drupal\media_delete_all\MediaDeleteService definition.
   *
   * @var \Drupal\media_delete_all\MediaDeleteService
   */
  protected $mediaDeleteAll;


  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entityTypeManager = $container->get('entity_type.manager');
    $mediaDeleteAll = $container->get('media_delete.all');

    return new static($entityTypeManager, $mediaDeleteAll);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct($entityTypeManager, $mediaDeleteAll) {
    $this->entityTypeManager = $entityTypeManager;
    $this->mediaDeleteAll = $mediaDeleteAll;
  }

  /**
   * Delete media.
   *
   * @param array $options
   *   An associative array of options whose values come from cli,
   *    aliases, config, etc.
   *
   * @option type
   *   pick media type
   * @usage drush delete-all-delete-media
   *   Delete media.
   *
   * @command delete:all-delete-media
   * @aliases dadm,delete-all-delete-media
   */
  public function allDeleteMedia(array $options = ['type' => NULL]) {
    // Initialize $media_type_options as FALSE to specify that all
    // content of all types should be deleted.
    // This will be overriden if user provides/choses a media type.
    $media_type_options = FALSE;

    // Check for presence of '--type' in drush command.
    if ($options['type']) {
      // func_get_args() collects all keywords separated by space in an array.
      // To get the content types, we join all the keywords in a string and then
      // use 'comma' to separate them.
      $types = $options['type'];
      if ($types != 1) {
        $media_types = $types;
        if (strpos($media_types, ',')) {
          $media_type_options = explode(',', $media_types);
        }
        else {
          $media_type_options = [$media_types];
        }
      }
      // Output all content types on screen and ask user to choose one.
      else {
        $media_type_options = [];
        $media_types = $this->entityTypeManager->getStorage('media_type')->loadMultiple();
        foreach ($media_types as $mediaType) {
          $choices[$mediaType->id()] = $mediaType->label();
        }

        $media_type_options = $this->io()->choice(dt("Choose a media type to delete. All contents of this"), $choices);

        // Return if no type is chosen.
        if ($media_type_options === 0) {
          return;
        }
        $media_type_options = [$media_type_options];
      }
    }

    if ($this->io()->confirm('Are you sure you want to delete the media?')) {
      // Get media to delete.
      $media_to_delete = $this->mediaDeleteAll->getMediaToDelete($media_type_options);
      // Get batch array.
      $batch = $this->mediaDeleteAll->getMediatDeleteBatch($media_to_delete);
      // Initialize the batch.
      batch_set($batch);
      // Start the batch process.
      drush_backend_batch_process();
    }
    else {
      throw new UserAbortException();
    }
  }

}
