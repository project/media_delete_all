<?php

namespace Drupal\media_delete_all;

use Drupal\media\Entity\Media;

/**
 * Create a Batch for deleting all media.
 */
class DeleteMediaBatchController {

  /**
   * {@inheritdoc}
   */
  public static function deleteAllMediaBatchDelete(array $medias, &$context) {
    $db = \Drupal::database();
    // Initialize sandbox.
    if (empty($medias)) {
      if (!isset($context['sandbox']['progress'])) {
        $context['sandbox']['progress'] = 0;
        $context['sandbox']['current_mid'] = 0;
        $context['sandbox']['max'] = $db->select('media', 'm')
          ->fields('m')
          ->countQuery()
          ->execute()
          ->fetchField();

        // Collect results to process in the finished callback.
        $context['results']['media_count'] = $context['sandbox']['max'];
      }

      $media_to_delete = $db->select('media', 'm')
        ->fields('m', ['mid'])
        ->condition('mid', $context['sandbox']['current_mid'], '>')
        ->range(0, 100)
        ->execute()
        ->fetchCol();
    }
    else {
      if (!isset($context['sandbox']['progress'])) {
        $context['sandbox']['progress'] = 0;
        $context['sandbox']['max'] = count($medias);
        // Collect results to process in the finished callback.
        $context['results']['media_count'] = $context['sandbox']['max'];
      }

      // Get a batch of 100 media to delete.
      $media_to_delete = array_slice($medias, $context['sandbox']['progress'], 100);
    }

    if ($context['sandbox']['max'] + 1 > 0) {
      if (!empty($media_to_delete)) {
        // ksm($media_to_delete);
        foreach ($media_to_delete as $mid) {
          // ksm($mid);
          // Delete media.
          $media = Media::load($mid);

          $media->delete();
          $context['message'] = t('Deleting Media with mid %mid', ['%mid' => $mid]);
          $context['sandbox']['current_mid'] = $mid;
          $context['sandbox']['progress']++;
        }
      }

      // Inform the batch engine that we are not finished,
      // and provide an estimation of the completion level we reached.
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
  }

  /**
   * Finished callback for the user deletion batch.
   *
   * @param int $success
   *   Equals 1 if batch is successfull else equals 0.
   * @param array $results
   *   List of results parameter collected during batch processing.
   * @param string $operations
   *   Todo to add description.
   */
  public static function deleteAllMediaBatchDeleteFinished($success, array $results, $operations) {
    if ($success) {
      \Drupal::messenger()->addMessage(t("Deleted @count medias.",['@count'=>$results['media_count']]));
    }
  }

}
