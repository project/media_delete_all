MEDIA DELETE ALL
----------
 
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module is used to delete all media from a site. This is mainly a 
developer tool,

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/media_delete_all

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/media_delete_all
 
REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 *  Configure the Media Delete All in Administration » Configuration »
    DEVELOPMENT :Batch Delete Media
    - Batch Delete Media -> Content then select all the media and delete them.
    - Batch Delete Media -> Content then select media type and delete them.


MAINTAINERS
-----------
 * Ashutosh Mishra - https://www.drupal.org/u/ashutoshmishra-0
